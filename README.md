# Godot demo projects

Each folder containing a `project.godot` file is a demo project meant to
be used with [Godot Engine](https://godotengine.org), the open source
2D and 3D game engine.

## Godot versions

- All the folder is compatible with Godot version 4.0.x
## Importing all demos

To import all demos at once in the project manager:

- Clone this repository or download as ZIP archive.
  - If you've downloaded a ZIP archive, extract it somewhere.
- Open the Godot project manager and click the **Scan** button on the right.
- Choose the path to the folder containing all demos.
- All demos should now appear in the project manager.

## Useful links

- [Main website](https://godotengine.org)
- [Source code](https://github.com/godotengine/godot)
- [Documentation](http://docs.godotengine.org)
- [Community hub](https://godotengine.org/community)
- [TPS demo](https://github.com/godotengine/tps-demo)

## License
Those demos are distributed under the terms of the Apache 2.0 license, as
described in the [LICENSE.md](LICENSE.md) file.
